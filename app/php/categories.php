<?php
require_once('adm_logged.php');
/**
 * @param $action
 * @param array $data
 * @return array
 */
function categories($action, $data = array()){
    switch($action){
        case 'selectAll':
            return SqlSelect('categories', 'active = 1 order by id_category, sort');
        case 'selectMain':
            return SqlSelect('categories', 'active = 1 and id_category = 0 order by sort');
        case 'selectByCategoryId':
            if(@$data['id_category'] != 0 && !@$data['id_category']) return Err('ID_CATEGORY_NULL');
            return SqlSelect('categories', 'active = 1 and id_category = ' . $data['id_category'] . ' order by sort');
        case 'create':
        case 'edit':
        case 'delete':
            isAdmLogged();
            return SqlInsert('categories', $data);
        default:
            return Err('ACTION NOT DEFINED OR NOT FOUND');
    }
}
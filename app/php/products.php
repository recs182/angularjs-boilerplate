<?php
require_once('adm_logged.php');

/**
 * @param $action
 * @param $data
 * @return array|mixed
 */
function products($action, $data = array()){
    switch($action){
        case 'multi':
            isAdmLogged();

            $id_product = SqlInsert('products', $data['products']);

            // IMAGES
            if(count($data['product_images'])){
                // delete all created images
                SqlQuery("DELETE FROM product_images WHERE id_product = $id_product");

                // ensure id_product is set
                foreach ($data['product_images'] as &$image){
                    $image['id_product'] = $id_product;
                }

                SqlInsert('product_images', $data['product_images']);
            }

            // CATEGORIES
            if(count($data['product_categories'])){
                // delete all created categories
                SqlQuery("DELETE FROM product_categories WHERE id_product = $id_product");

                $categories = array();
                foreach ($data['product_categories'] as $id_category){
                    $category = array("id" => 0, "id_product" => $id_product, "id_category" => $id_category);
                    array_push($categories, $category);
                }
                SqlInsert('product_categories', $categories);
            }
            return $id_product;
        case 'selectByProductId':
            $id_product = @$data['id_product'];
            if(!$id_product){
                return Err('ID_PRODUCT_NULL');
            }

            $data = [];
            $arr_product = SqlSelect('products', "id = $id_product");

            if(!count($arr_product)){
                return Err('PRODUCT_NOT_FOUND');
            }

            $data['products'] = $arr_product[0];
            $data['product_categories'] = SqlQuery("SELECT c.* FROM categories c LEFT JOIN product_categories pc on pc.id_category = c.id WHERE pc.id_product = $id_product ORDER BY sort");
            $data['product_images'] = SqlSelect('product_images', "id_product = $id_product ORDER BY sort");

            return $data;
        case 'selectByCategoryId':
            $id_category = @$data['id_category'];
            if(!$id_category){
                return Err('IDS_CATEGORY_NULL');
            }
            return SqlQuery("SELECT p.*, GROUP_CONCAT(DISTINCT pi.image ORDER BY sort SEPARATOR \"!@!\") as images FROM products p LEFT JOIN product_categories pc on pc.id_product = p.id LEFT JOIN product_images pi on pi.id_product = p.id WHERE pc.id_category in ($id_category) and p.active = 1 GROUP BY p.id ORDER BY p.id");
        case 'selectAll':
            return SqlQuery("SELECT p.*, GROUP_CONCAT(DISTINCT pi.image ORDER BY sort SEPARATOR \"!@!\") as images FROM products p LEFT JOIN product_images pi on pi.id_product = p.id WHERE p.active = 1 GROUP BY p.id ORDER BY p.id");
        case 'delete':
            isAdmLogged();
            $id_product = @$data['id_product'];
            if(!$id_product){
                return Err('ID_PRODUCT_NULL');
            }

            $delete = array("id" => $id_product, "active" => 0);
            return SqlInsert('products', $delete);
        default:
            return Err('ACTION NOT DEFINED OR NOT FOUND');
    }
}
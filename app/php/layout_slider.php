<?php
require_once('adm_logged.php');

/**
 * @param $action
 * @param $data
 * @return array|mixed
 */
function layout_slider($action, $data){
    switch($action){
        case 'selectAll':
            return SqlSelect('layout_slider', 'active = 1 order by sort');
        case 'create':
        case 'delete':
            isAdmLogged();
            return SqlInsert('layout_slider', $data);
        default:
            return Err('ACTION NOT DEFINED OR NOT FOUND');
    }
}
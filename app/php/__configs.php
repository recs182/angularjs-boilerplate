<?php
$configs = array(
    'APPROVAL' => array(
        'active' => true,
        'url' => 'http://192.168.1.35/angularjs-boilerplate/dist/' // sempre com www
    ),
	'COMPANY' => array(
		'name' => 'Development',
		'description' => '',
		'email' => '',
		'phone' => '',
		'whatsapp' => '',
		'zipcode' => '',
		'city' => '',
		'state' => '',
		'address' => '',
		'neighbor' => '',
		'cnpj' => '',
		'URL' => array(
			'base' => '',
			'ssl' => false
		),
		'KEYS' => array(
			'google' => '',
			'facebook_pixel' => ''
		)
	),
	'METATAGS' => array(
		'geo_placename' => '',
		'geo_region' => '',
		'theme_color' => '#000000'
	)
);

$_SESSION['CONFIGS'] = $configs;
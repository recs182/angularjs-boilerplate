<?php
require_once('adm_logged.php');

function form($action, $data = array()){
    switch($action){
        case 'selectAll':
            isAdmLogged();
            return SqlSelect('forms', 'active = 1');
        case 'createClient':
            return SqlInsert('forms', $data);
        case 'delete':
            isAdmLogged();
            return SqlInsert('forms', $data);
        default:
            return Err('ACTION NOT DEFINED OR NOT FOUND');
    }
}
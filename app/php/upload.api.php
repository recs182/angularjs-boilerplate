<?php

function toSlug2($string) {
    $table = array(         'Š'=>'S', 'š'=>'s', '&#272;'=>'Dj', '&#273;'=>'dj', 'Ž'=>'Z',         'ž'=>'z', '&#268;'=>'C', '&#269;'=>'c', '&#262;'=>'C', '&#263;'=>'c',         'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',         'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',         'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',         'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',         'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',         'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',         'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',         'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',         'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',         'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',         'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',         'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',         'ÿ'=>'y', '&#340;'=>'R', '&#341;'=>'r',     );
	// Traduz os caracteres em $string, baseado no vetor $table 
	$string = strtr($string, $table);
	
	// converte para minúsculo 
	$string = strtolower($string);
	// remove caracteres indesejáveis (que não estão no padrão) 
	$string = preg_replace("/[^a-z0-9_.\s-]/", "", $string);
	// Remove múltiplas ocorrências de hífens ou espaços 
	$string = preg_replace("/[\s-]+/", " ", $string);
	// Transforma espaços e underscores em hífens 
	$string = preg_replace("/[\s_]/", "-", $string);
	// retorna a string 
	return $string;
}
function upload( $file, $dest ){
	$return = array();
	foreach( range(0, count($_FILES[$file]['name'])-1) as $index ){
		$uploadfile = toSlug2( basename($_FILES[$file]['name'][$index] ) );	
		$ext = pathinfo($uploadfile, PATHINFO_EXTENSION);
		if(in_array($ext, array("php","js","css","html"))) return;
		$path       = $dest;
		$path_file  = str_replace('\\','/', $path.'/'.$uploadfile );
		if (move_uploaded_file($_FILES[$file]['tmp_name'][$index], $path_file )) {
		    $return[] = $path_file;
		} else {
		    return false;
		}
	}
	return $return;
}

?>
<?php
header('Content-Type: text/html; charset=utf-8');

$__connection = null;
$__query_log  = false;

/**
 * @param  array $arr
 * @return boolean
 */
function isAssoc($arr){
	return array_keys($arr) !== range(0, count($arr) - 1);
}


function escapeNoConnection($value){
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
    return str_replace($search, $replace, $value);
}
/**
 * @param $str
 * @return mixed
 */
function escape($str){
	global $__connection;
	try{
        return $__connection->real_escape_string($str);
    }catch(Exception $exception){
        escapeNoConnection($str);
    }
}

function SqlLog(){
	global $__query_log;
	$__query_log = true;
}

function SqlLogLast(){
	global $__query_log;
	echo $__query_log[count($__query_log)-1];
}

/**
 * @param $address
 * @param $user
 * @param $pass
 */
function SqlHost($address, $user, $pass ){
	global $__connection;
	$__connection = new mysqli($address, $user, $pass);
	if ($__connection->connect_errno){
	    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$__connection->set_charset("utf8");
}

/**
 * @param $address
 * @param $user
 * @param $pass
 * @param $db
 */
function SqlConnect($address, $user, $pass, $db){
	global $__connection;
	$__connection = new mysqli($address, $user, $pass, $db);
	if ($__connection->connect_errno){
	    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$__connection->set_charset("utf8");
	$timezone = date('Z');
	$timezone = gmdate( ($timezone > 0 ? "" : "-") . "H:i", abs($timezone));
	$__connection->query('SET GLOBAL time_zone = "'.$timezone.'"');
	// $__connection->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
}

/**
 * @param $dbname
 */
function SqlSelectDb($dbname){
	global $__connection;
	mysqli_select_db($__connection,$dbname) or die("Não foi possível encontar o banco");
}
/*
$types : "assoc", "row", "object", "array"
*/
/**
 * @param $query
 * @param string $type
 * @return array
 */
function SqlQuery($query, $type = "assoc"){
	global $__connection, $__query_log;
	if($__query_log){
		$q = trim($query);
		$subtype = substr($q,0, strpos($q, ' '));
		$d = date("H:i:s d/m/Y");
		if(@$_SESSION['admin_logged']){
			$sql = 'insert into logger values( 0, "SQL", "'.$subtype.'", "'.$_SESSION['id_admin'].'", "'.$d.'" ,"'.$q.'" )';
			$__connection->query($sql);
		}
	}
	$fetch = "fetch_".$type;
	$results = $__connection->query($query);
    $res = array();
    if($results){
		if(is_object($results)) {
			while($row = $results->$fetch()) $res[]=$row;
		}
    }else{
    	echo "Mysql Error: ", $__connection->error, '('.$query.')';
    }
    if( strpos($query, "insert into") === 0 ) return $__connection->insert_id;
    return $res;
}

/**
 * @param $table
 * @param $json
 * @param bool $show
 * @return array|mixed
 */
function SqlInsert($table, $json, $show = false ){
	$return_array = false;
	if( isAssoc( $json ) ){
		$jsons[] = $json;
	}else{
		$return_array = true;
		$jsons = $json;
	}
	$keys   			= array();

	foreach($jsons[0] as $k=>$v){
		$keys[] 		= $k;
	}

	$fields 	= implode(',', $keys);
	$fields_no_id = $keys;
	if(($key = array_search("id", $fields_no_id)) !== false) {
    	unset($fields_no_id[$key]);
	}
	$key_values = array();
	foreach($fields_no_id as $key){
		$key_values[] = $key .'= values('.$key.')';
	}

	$key_values = implode(",", $key_values);
	$return_ids = array();
	// $return_ids[] = @$jsons[0]['id'] ? $jsons[0]['id'] : null;
	$values_ = '(\''.implode('\',\'',array_values( $jsons[0] )).'\')';


	$values_length = strlen($values_);
	$total_length = $values_length * count($jsons);
	$total_chunks = ceil( 14000000 / $total_length  );

	foreach( array_chunk($jsons, $total_chunks ,false) as $json ){
		$values_ = '(\''.implode('\',\'',array_values( $json[0] )).'\')';
		$return_ids[] = @$json[0]['id'] ? $json[0]['id'] : null;
		for($i = 1 ;$i < count($json); ++$i){
			$j = $json[$i];
			$return_ids[] = @$j['id'] ? $j['id'] : null;
			$values_ .= ',(\''.implode('\',\'',array_values($j)).'\')';
		}

		$query = 'insert into '.$table.' ('.$fields.') values '.$values_.' on duplicate key update '.$key_values;
		if($show) echo $query;
		$return_id = SqlQuery( $query );
		foreach($return_ids as &$rid){
			if($rid == null){
				$rid = $return_id++;
			}
		}
	}
	return $return_array ? $return_ids : $return_ids[0] ;
}

/**
 * @param $table
 * @param array $sets
 * @param string $conds
 * @return array
 */
function SqlUpdate($table, $sets = array(), $conds = ""){
	global $__connection;
	if( !isAssoc( $sets ) ){
		return SqlUpdateById( $table, $sets );
	}else{
		$tmp = array();
		foreach( $sets as $key => $val ){
			$tmp[] = "$key = ". ( is_string( $val ) ? "'$val'" : "$val" );
		}
		$values = implode(",", $tmp);
		$query = "update $table set $values ". ($conds ? "where $conds" : "");
		return SqlQuery($query);
	}
}

/**
 * @param $table
 * @param array $sets
 * @return array
 */
function SqlUpdateById($table, $sets = array()){
	$ids = array();
	foreach($sets as $update){
		$ids[] = SqlUpdate( $table, $update, "id = ".$update["id"] );
	}
	return $ids;
}

/**
 * @param $table
 * @param string $conds
 * @return array
 */
function SqlDelete($table, $conds = "" ){
	return SqlQuery("delete from $table ". ($conds ? "where $conds" : "") );
}

/**
 * @param $table
 * @param string $conds
 * @return array
 */
function SqlSelect($table, $conds = "" ){
	return SqlQuery("select * from $table ". ($conds ? "where $conds" : "") );
}

/**
 * @param $table
 * @param string $conds
 * @return array
 */
function SqlSelectCompress($table, $conds = "" ){
	$fields = array();
	foreach(SqlFields( $table ) as $f){
		$fields[] = $f["name"];
	}
	return array( "fields" => $fields, "values" => SqlQuery("select * from $table ". ($conds ? "where $conds" : ""), "row" ) );
}

/**
 * @return array
 */
function SqlScheme(){
	$tables = array();
	foreach(SqlQuery("show tables","array") as $r){
		$table = $r[0];
		$tables[] = array( "table" =>$table , "fields" => SqlFields($table, true) );
	}
	return $tables;
}

/**
 * @return array
 */
function SqlTables(){
	$tables = array();
	foreach(SqlQuery("show tables", "array") as $arr){
		$tables[] = $arr[0];
	}
	return $tables;
}

/**
 * @param $table
 * @param bool $exclude_id
 * @return array
 */
function SqlFields($table, $exclude_id = false ){
	$fields = array();
	foreach(SqlQuery("show fields from $table", "array") as $r2){
		if( $exclude_id && $r2[0] == "id" )continue;
		$fields[] = array( "name" => $r2[0], "type" => $r2[1] );
	}
	return $fields;
}

/**
 * @param $data
 * @return string
 */
function left_join($data){ return 'left join '.$data['from'].' on '.$data['on']; };
/**
 * @param $data
 * @return string
 */
function right_join($data){ return 'right join '.$data['from'].' on '.$data['on']; };

/**
 * Class SqlBuild_
 */
class SqlBuild_{
	private $selects 	= array();
	private $froms		= array();
	private $left_join	= array();
	private $right_join	= array();
	private $wheres    	= array();
	private $whereors  	= array();
	private $limits     = array();
	private $orders    	= array();
	private $groups    	= array();
	private $having    	= array();

	function select( $str ){
		$this->selects[] = $str;
		return $this;
	}
	function from($str){
		$this->froms[] = $str;
		return $this;
	}
	function leftJoin($from, $on){
		$this->left_join[] = array( 'from'=> $from, 'on'=>$on );
		return $this;
	}
	function rightJoin($str){
		$this->right_join[] = $str;
		return $this;
	}
	function where($str){
		$this->wheres[] = $str;
		return $this;
	}
	function whereOr( $str, $group ){
		$this->whereors[$group][] = $str;
		return $this;
	}
	function order($str){
		$this->orders[] = $str;
		return $this;
	}
	function group($str){
		$this->groups[] = $str;
		return $this;
	}
	function limit($limit_offset){
		$aux = explode(',', $limit_offset);
		$this->limits = array($aux[0], @$aux[1]);
		return $this;
	}
	function having($str){
		$this->having[] = $str;
		return $this;
	}
	function query($show = false){


		foreach( $this->whereors as $group=>$wheres){
			$this->wheres[] = '( ' .implode(" or ", $wheres) .' )';
		}

		if( !$this->selects )$this->selects = array('*');

		$sql_arr = array(
			' select '.implode(',',$this->selects),
			' from '.implode(',', $this->froms) ,
			implode(' ',array_map('left_join',$this->left_join) ),
			implode(' ',array_map('right_join',$this->right_join) ),
			$this->wheres ? 'where '.implode(' and ', $this->wheres) : '',
			$this->groups ? 'group by '.implode(',', $this->groups) : '',
			$this->having ? 'having '.implode(' and ', $this->having) : '',
			$this->orders ? 'order by '.implode(',', $this->orders) : '',
			@$this->limits[0] ? ' limit '.$this->limits[0] : '',
			@$this->limits[1] ? ' offset '.$this->limits[1] : ''
		);
		if($show) Console( implode(' ', $sql_arr) );
		return SqlQuery( implode(' ', $sql_arr) );
	}
}

/**
 * @return SqlBuild_
 */
function SqlBuild(){
	return new SqlBuild_();
}

/*
WEB SERVICE UTIL
*/
/**
 * @param int $code
 * @param array $data
 * @return array
 */
function Err($code = 0, $data = array() ){
    http_response_code(400);
    return array("code" => $code, "data" => $data);
}
$GLOBALS['__jslog'] = array();
/**
 * @param $string
 */
function Console($string ){ global $__jslog; $__jslog[]= $string;  }

/**
 *
 */
function WebService(){
	global $__jslog;
	$POST = json_decode(file_get_contents('php://input'),true);
	if( @$_REQUEST['__action'] ) {
		$POST['__action'] = $_REQUEST['__action'];
	}
	$action = @$POST['__action'];

	if($action){
		if(file_exists($action.'.php')){
			include($action.'.php');
		}
		if(!function_exists($action)){
			echo json_encode( array('err' => 'Action undefined') );
			return;
		}
		$refFunc 	 = new ReflectionFunction($action);
		$arguments   = array();

		foreach( $refFunc->getParameters() as $param ){
			$arg = $param->getName();
			$arguments[$arg] = @$POST[$arg];
		}

		$r = call_user_func_array($action, $arguments);
		if($__jslog){
			$r['__jslog'] = $__jslog;
		}
		echo json_encode($r);
	}

}
?>
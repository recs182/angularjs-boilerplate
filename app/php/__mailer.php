<?php
require('PHPMailer-master/class.phpmailer.php');

function php_mailer($data){
	$host         = '';
	$username     = '';
	$password     = '';
	$from         = '';
	$from_name    = '';
	$port         = '';
	
	$destiny = $data['destinatario'];
	$subject = $data['assunto'];
	$content = $data['conteudo'];

	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->IsHTML(true);
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = ''; // ssl, tls ou vazio
	$mail->SetLanguage('br', 'PHPMailer-master/language/');

	$mail->Host     = $host;
	$mail->Username = $username;
	$mail->Password = $password;
	$mail->From     = $from;
	$mail->FromName = $from_name;
	$mail->Port     = $port;	

	$mail->charSet = 'UTF-8'; 
	$mail->AddAddress($destiny);
	$mail->Subject = utf8_decode($subject);
	$mail->Body    = $content;

	if(!$mail->Send()) {
		echo $mail->ErrorInfo;
	}

	$mail->ClearAddresses();
	$mail->ClearAttachments();
	$mail->ClearAddresses();
	$mail->ClearAllRecipients();
	$mail->ClearBCCs();
	$mail->ClearCustomHeaders();

	echo 'Enviado';
}
<?php
/**
 * @param $action
 * @param $email
 * @param $password
 * @return array|bool|null
 */
function adm_login($action, $email, $password){
	$email = trim($email);
	$password = trim($password);

	switch($action){
		case 'login':
            // veritifications
            if(!@$email){
                return Err('EMAIL_NULL');
            }
            if(!@$password){
                return Err('PASSWORD_NULL');
            }

            $email = escape($email);
            $password = escape($password);
            $password = md5( $password . md5($password) );

            $result = SqlSelect('administrators', "email = '$email' and password = '$password'");

            if($result){
                $administrator = $result[0];
                if(!$administrator['active']){
                    return Err('ADMINISTRATOR_INACTIVE');
                }

                $_SESSION['administrator_logged'] = true;
                $_SESSION['administrator_id'] 	  = $administrator['id'];

                return true;
            }else{
                return Err('LOGIN_FAILED');
            }
            break;
        case 'isLogged':
            if(@$_SESSION['administrator_logged']){
                return true;
            }else{
                return Err('ADMINISTRATOR_DISCONNECTED');
            }
            break;
        case 'logout':
            $_SESSION['administrator_logged'] = false;
            $_SESSION['administrator_id']     = 0;
            return null;
            break;
    }
}
?>

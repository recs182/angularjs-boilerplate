class sAdmLogin{
	constructor(sAjax, $q){
		this.ajax = sAjax;
		this.q    = $q;
	}

	login(email, password){
		return this.q((resolve, reject) => {
			const action = 'login';
			this.ajax.post('adm_login', {action, email, password})
			.then(data => resolve(data ))
			.catch(reject);
		})
	}

	isLogged(){
		return this.q((resolve, reject) => {
			const action = 'islogged';
			this.ajax.post('adm_login', {action})
			.then(data => resolve(data))
			.catch( reject );
		})
	}

	logout(){
		return this.q((resolve, reject) => {
			const action = 'logout';
			this.ajax.post('adm_login', {action})
			.then(data => resolve(data))
			.catch(reject);
		})
	}
}
class sEmail{
	constructor(sAjax, $q){
		this.ajax = sAjax;
		this.q    = $q;
	}
	
	send(to, template, vars = {}){
		return this.q( (resolve, reject)=>{
			this.ajax.post('generic_email', {to, template, vars})
			.then(data => resolve(data))
			.catch(reject);		
		})
	}
}
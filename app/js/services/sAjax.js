class sAjax{
	
	constructor($http, $q, CONFIGS){
		this.http = $http;
		this.q = $q;
		this.CONFIGS = CONFIGS;
	}

	post(action, args = {}){
		const ext = urlExtension(this.CONFIGS.APPROVAL);
		return new this.q((resolve, reject) => {
			args.__action = action;

			this.http.post(ext + 'php/__ajax.php', args).then(data => {
				if( data.data.__jslog ){
					data.data.__jslog.forEach(e => { console.log(e)});
				}
				delete data.data.__jslog;
				return data.data.err ? reject(data.data) : resolve(data.data)
			}).catch(reject)
		});
	}
}


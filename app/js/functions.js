function urlExtension(approval){
    let url        = location.href;
    let regexUrl   = /(http:\/\/|https:\/\/)(.*)/i;
    let regexDev   = /(192\.168\.1|127\.0\.0|localhost)/i;
    let regexLocal = /(192\.168\.1|127\.0\.0|localhost)/i;

    // PRODUCTION
    if(!url.match(regexUrl)) return '/';

    let [complete, prefix, parcial, index, input] = url.match(regexUrl);
    let matchDev                                  = parcial.match(regexDev);
    let matchLocal                                = parcial.match(regexLocal);

    if(matchDev){
        let splitted_parcial = parcial.split('/');

        // LOCAL DEVELOPMENT
        if(matchLocal){
            let joins = [];
            for(let splits of splitted_parcial){
                joins.push(splits);
                if(splits.match(/dist/)) break;
            }
            return prefix + joins.join('/') + '/';
        }

        // ONLINE DEVELOPMENT (UNDER APPROVAL) / PATTERN: PROTOCOL_PREFIX + DOMAIN + ANY_FOLDER + PROJECT_FOLDER
        if(!splitted_parcial[1]) return prefix + splitted_parcial[0] + '/';
        if(!splitted_parcial[2]) return prefix + splitted_parcial[0] + '/' + splitted_parcial[1] + '/';
        return prefix + splitted_parcial[0] + '/' + splitted_parcial[1] + '/' + splitted_parcial[2] + '/';
    }
    // PRODUCTION FALLBACK
    if(approval.active) return approval.url;
    return '/';
}

function swap(arr, x, y){
    let b  = arr[x];
    arr[x] = arr[y];
    arr[y] = b;
    return arr;
}

function searchArray(arr, field, value){
    for(let index in arr){
        let val = arr[index];
        if(val[field] == value) return val;
    }
}

function searchArrayMultiple(arr, field, value){
    let result = [];
    for(let index in arr){
        let val = arr[index];
        if(val[field] == value) result.push(val);
    }
    return result;
}

function copy(o) {
    let output, v, key;
    output = Array.isArray(o) ? [] : {};
    for (key in o) {
        v = o[key];
        output[key] = (typeof v === "object") ? copy(v) : v;
    }
    return output;
}

function isLocalhost(url){
    const testRegex = /192\.168\.1|127\.0\.0|localhost/i;
    return url.match(testRegex) ? true : false;
}

function resolveStructure(arr, field){
    const structure = arr.filter(item => Number(item[field]) === 0);

    // sub level
    if(structure.length){
        structure.forEach(main => {
            main.items = searchArrayMultiple(arr, field, main.id);

            // last level
            if(main.items.length){
                main.items.forEach(item => {
                    item.items = searchArrayMultiple(arr, field, item.id);
                })
            }
        })
    }
    return structure;
}
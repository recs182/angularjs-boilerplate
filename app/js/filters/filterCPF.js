function filterCPF(){
    return (cpf) => {
        const formatter = new StringMask('000.000.000-00');
        return formatter.apply(cpf);
    }
}
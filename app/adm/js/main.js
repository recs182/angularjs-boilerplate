angular.module('app', [
    'app.filters',
	'ui.router',
	'ngSanitize',
	'ui.bootstrap',
	'ui.utils.masks',
	'monospaced.elastic',
	'ngToast',
	'html5.sortable',
	'ngQuill',
    // 'ngHandsontable',
])
.run(($rootScope, $document, $window, $state, sAdmLogin, CONFIGS)=>{
    $rootScope.CONFIGS = CONFIGS;
    $rootScope.scroll = {location: 0, blocked: false};
    $rootScope.current_page = '';

    // aside
    $rootScope.asideOpen = false;
    $rootScope.toggleAside = function(){
        $rootScope.asideOpen = !$rootScope.asideOpen;
        //block main scroll
        $rootScope.scroll.blocked = !$rootScope.scroll.blocked;
    };

    // logout method
    $rootScope.logout = function(){
        sAdmLogin.logout().then(() => {
            location.reload();
        },
        err => {
            console.log("err", err);
        })
    };

    $document.on('scroll', function() {
        $rootScope.$apply(function() {
            $rootScope.scroll.location = $window.scrollY;
        })
    });

    $rootScope.$on('$stateChangeSuccess', ()=>{
        document.body.scrollTop = document.documentElement.scrollTop = 0;

        $rootScope.asideOpen = false;
        $rootScope.scroll.blocked = false;
        $rootScope.current_page = $state.current.name.replace(/[.]/gi, '-');
    });
})
.config(($stateProvider, $urlRouterProvider, $locationProvider)=>{
 	isLocalhost(location.href) ? $locationProvider.html5Mode(false) : $locationProvider.html5Mode(true);
 	$urlRouterProvider.otherwise('/');

	$stateProvider
	.state('main', {
		templateUrl: 'views/structure/main.html',
	})	
	.state('main.panel', {
		url: '/',
		templateUrl: 'views/panel/panel.html',
        controller: 'cPanel',
        controllerAs: '$ctrl'
	})
	.state('main.404', {
		url: '/404',
		templateUrl:'views/error/404.html',
	})
    // Section: Products
    .state('main.product', {
        url: '/produto',
        template: '<ui-view></ui-view>',
    })
        .state('main.product.categories', {
            url: '/categorias',
            templateUrl: 'views/product/categories/categories.html',
            controller: 'cProductCategories',
            controllerAs: '$ctrl',
        })
        .state('main.product.list', {
            url: '/listar',
            templateUrl: 'views/product/list/list.html',
            controller: 'cProductList',
            controllerAs: '$ctrl',
        })
        .state('main.product.multi', {
            url: '/multi/{id_product:.*}',
            templateUrl: 'views/product/multi/multi.html',
            controller: 'cProductMulti',
            controllerAs: '$ctrl',
        })
    // Section: Customization
    .state('main.customization', {
        url: '/personalizar',
        template: '<ui-view></ui-view>',
    })
        .state('main.customization.slider', {
            url: '/slider',
            templateUrl: 'views/customization/slider/slider.html',
            controller: 'cCustomizationSlider',
            controllerAs: '$ctrl',
        })
})
//////////////////////////////////////////
// Controllers
//////////////////////////////////////////
    .controller('cLogin', cLogin)
    .controller('cAside', cAside)
    .controller('cPanel', cPanel)
    .controller('cProductCategories', cProductCategories)
    .controller('cProductCategoriesOrder', cProductCategoriesOrder)
    .controller('cProductList', cProductList)
    .controller('cProductMulti', cProductMulti)
    .controller('cProductMultiUploadImage', cProductMultiUploadImage)
    .controller('cCustomizationSlider', cCustomizationSlider)
    .controller('cCustomizationSliderImageZoom', cCustomizationSliderImageZoom)

//////////////////////////////////////////
// Components
//////////////////////////////////////////
    .component('pageHeader', PageHeader)
    .component('uploadFiles', UploadFiles)
    .component('pageLoading', PageLoading)

//////////////////////////////////////////
// Directives
//////////////////////////////////////////
    .directive('dErrSrc', () => new errSrc())

//////////////////////////////////////////
// Services
//////////////////////////////////////////
    .service('sAjax', sAjax)
    .service('sAdmLogin', sAdmLogin)
class cPanel{

    constructor(sAjax){
        this.sAjax = sAjax;

        this.forms = [];

        this.init();
    }

    init(){
        this.sAjax.post('form', {action: 'selectAll'}).then(res => {
            res.forEach(row => {
                row.form = JSON.parse(row.form);
            });
            this.forms = res;
        }, err => {
            console.log(err);
        })
    }

    deleteNews(id){
        this.sAjax.post('form', {action: 'delete', data: {id: id, active: 0}}).then(() => {
            this.init();
            swal({ title: 'Email removido', text: 'O email foi removido com sucesso', type: 'success', showConfirmButton: true });
        }, () => {
            swal({ title: 'Falha ao remover', text: 'Tente novamente em alguns instantes', type: 'error', showConfirmButton: true });
        })
    }
}
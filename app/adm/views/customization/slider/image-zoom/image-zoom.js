class cCustomizationSliderImageZoom{

    constructor($uibModalInstance, data){
        this.uibModalInstance = $uibModalInstance;

        this.data = data;
    }

    close(){
        this.uibModalInstance.close();
    }

}

class cCustomizationSlider{
    constructor(sAjax, ngToast, $uibModal){
        this.sAjax = sAjax;
        this.ngToast = ngToast;
        this.uibModal = $uibModal;

        this.loaded = false;
        this.sliders = [];
        this.new = {image: '', name: ''};

        this.init();
    }

    init(){
        this.sAjax.post('layout_slider', {action: 'selectAll'}).then(res => {
            this.sliders = res;
            this.loaded = true;
        }, err => {
            console.log('create:', err);
        })
    }

    create(){
        // inject creation date, sort number
        this.new.creation = new Date().getTime();
        this.new.sort = this.sliders.length;

        if(!this.new.image){
            this.ngToast.create({className: 'danger', content: 'Escolha uma imagem antes de cadastrar'});
            return;
        }

        this.sAjax.post('layout_slider', {action: 'create', data: this.new}).then(() => {
            this.new = {image: '', name: ''};
            this.init();
        }, err => {
            console.log('create:', err);
        })
    }

    delete(slider){
        const confirmation = confirm(`Tem certeza que deseja deletar o slider ${slider.name}?`);

        if(confirmation){
            this.sAjax.post('layout_slider', {action: 'delete', data: {id: slider.id, active: 0 }}).then(() => {
                this.ngToast.create({className: 'info', content: 'Slider removido'});
                this.init();
            }, err => {
                this.ngToast.create({className: 'danger', content: 'Erro ao remover slider'});
                console.log('remove:', err);
            })
        }
    }

    imageZoom(image){
        this.uibModal.open({
            templateUrl: 'views/customization/slider/image-zoom/image-zoom.html',
            controller: 'cCustomizationSliderImageZoom',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                data: () => image
            },
        });
    }
}
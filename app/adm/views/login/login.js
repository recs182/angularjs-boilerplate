class cLogin{

	constructor($state, sAdmLogin){
		this.state  = $state;
		this.sAdmLogin = sAdmLogin;
	}

	authLogin(data){
		const email = data.email;
		const password = data.password;

		this.sAdmLogin.login(email, password).then(() => {
			location.reload();
		}, err => {
			//TODO: make visual error for EMAIL_NULL, PASSWORD_NULL and ADMINISTRATOR_INACTIVE
			console.log("err", err);
			swal({ title: 'Email ou senha errados.', text: 'Verifique os dados e tente novamente.', type: 'error', showConfirmButton: true });
		});
	}

}
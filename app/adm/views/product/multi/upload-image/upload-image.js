class cProductMultiUploadImage{

    constructor($rootScope, $uibModalInstance, data){
        this.rootScope = $rootScope;
        this.uibModalInstance = $uibModalInstance;

        this.image = '';
        this.index = data;
        this.sortableMode = 'on';

        $rootScope.$watch(() => this.image, (newVal, oldVal) => {
            if(newVal){
                this.confirm();
            }
        })
    }

    confirm(){
        this.rootScope.$broadcast('UPLOAD_PRODUCT_IMAGE', {image: this.image, index: this.index});
        this.close();
    }

    close(){
        this.uibModalInstance.close();
    }

}

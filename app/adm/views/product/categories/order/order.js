class cProductCategoriesOrder{

    constructor($rootScope, $uibModalInstance, sAjax, ngToast, data){
        this.rootScope = $rootScope;
        this.uibModalInstance = $uibModalInstance;
        this.ngToast = ngToast;
        this.sAjax = sAjax;

        this.data = data;
        this.sortableMode = 'on';
    }

    confirm(){

        const update = this.data.levels.map((value, index) => {
            return {
                id: value.id,
                sort: index,
            }
        });

        this.sAjax.post('categories', {action: 'edit', data: update}).then(() => {
            this.ngToast.create({content: '<i class="fa fa-check"></i> Categorias ordenadas'});
            this.rootScope.$broadcast('REORDER_CATEGORIES', this.data.currentLevel);
            this.close();
        }, err => {
            console.log('confirm: ' , err);
        })
    }

    close(){
        this.uibModalInstance.close();
    }

}

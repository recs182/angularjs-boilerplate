class cProductList{
    constructor(sAjax){
        this.sAjax = sAjax;

        this.loaded = false;
        this.search = {texts: ''};
        this.products = [];

        this.init();
    }

    init(){
        this.sAjax.post('products', {action: 'selectAll'}).then(res => {
            this.products = res.map(product => {
                product.image = product.images ? product.images.split('!@!')[0] : '';
                return product;
            });
            this.loaded = true;
        }, err => {
            console.log('init', err);
        })
    }
}
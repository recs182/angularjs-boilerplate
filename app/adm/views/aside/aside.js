class cAside{

    constructor(sAjax){
        this.sAjax = sAjax;

        this.pages = [];

        this.init();
    }

    init(){
        this.sAjax.post('adm_pages', {action: 'selectAll'}).then(res => {
            this.pages = resolveStructure(res, 'id_feature');
        }, err => {
            console.log('err', err);
        });
    }
}
class cProductList{
    constructor($stateParams, $rootScope, sAjax, ngMeta){
        this.stateParams = $stateParams;
        this.rootScope = $rootScope;
        this.sAjax = sAjax;
        this.ngMeta = ngMeta;

        this.link = this.stateParams.link;

        this.listLinks = {};
        this.categories = [];
        this.products = [];

        this.init();
    }

    init(){
        this.sAjax.post('categories', {action: 'selectAll'}).then(res => {
            res.forEach(category => {
                this.listLinks[category.link] = category.id;
            });

            //save current link id
            sessionStorage.productList_idCategory = this.listLinks[this.link] || 0;

            if(this.link === 'todos'){
                const multipleIds = Object.values(this.listLinks).join(',');
                this.getProducts(multipleIds);
            }else{
                const singleId = this.listLinks[this.link];
                this.getProducts(singleId);
            }

            const title = this.link === 'todos' ? 'Todos' : searchArray(res, 'link', this.link).name;
            this.ngMeta.setTitle(`Categoria: ${title}`);
            this.ngMeta.setTag('description', `Você está os produtos da categoria ${title} de nosso catálogo.`);


            this.categories = resolveStructure(res, 'id_category');
        }, err => {
            console.log('err', err);
        })
    }

    getProducts(ids){
        this.sAjax.post('products', {action: 'selectByCategoryId', data: {id_category: ids}}).then(res => {
            this.products = res;
        }, err => {
            console.log('err', err);
        })
    }

}
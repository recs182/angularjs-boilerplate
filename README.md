# AngularJS boilerplate Macro Publicidade

### Getting started
- make a clone of this repository `git clone https://bitbucket.org/recs182/angularjs-boilerplate.git project_name`
- delete .git `rm -rf .git`
- create `dist` folder (mkdir dir)
- install packages `npm i`
- run gulp in a terminal
- see your project at `dist` folder, edit then in `app` folder

### Known Issues
- Gulp related: When running `gulp` for the first time in the console it'll trown an error saying that the folder `dist` doesn't exist, so just be sure that before you start working the folder `dist` was created *(mkdir dist)*.
